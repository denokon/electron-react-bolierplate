import {from, Observable, ObservableInput} from 'rxjs';

export function properExhaustMap<T, TResult>(project: (v: T) => ObservableInput<TResult>)
{
    return (source: Observable<T>) => new Observable<TResult>(observer => {
        let inProgress = false;
        let hasLastValue = false;
        let lastValue = null;

        return source.subscribe({
            next: async value => {
                try
                {
                    hasLastValue = true;
                    lastValue = value;

                    if (inProgress)
                        return;

                    inProgress = true;
                    do
                    {
                        hasLastValue = false;
                        
                        const obs = from(project(lastValue));
                        obs.subscribe(text => observer.next(text));
                        await obs.toPromise();
                    } while (hasLastValue);
                    inProgress = false;
                }
                catch (err)
                {
                    observer.error(err);
                }
            },
            error(err)
            {
                observer.error(err);
            },
            complete()
            {
                observer.complete();
            }
        });
    });
}