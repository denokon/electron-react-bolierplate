import {Configuration, HotModuleReplacementPlugin} from 'webpack';

import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
// @ts-ignore
import CircularDependencyPlugin from 'circular-dependency-plugin';

import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const distDir = path.join(__dirname, '../../dist');
const distFile = 'renderer.js';

const config: Configuration = {
    target: 'electron-renderer',
    
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    
    entry: './src/index.tsx',
    devtool: 'eval-source-map',
    mode: 'development',
    output: {
        path: distDir,
        filename: distFile
    },
    devServer: {
        stats: 'minimal',
        historyApiFallback: {
            disableDotRule: true
        },
        hot: true,
        overlay: {
            warnings: true,
            errors: true
        }
    },

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    { loader: 'cache-loader' },
                    {
                        loader: 'thread-loader',
                        options: {
                            workers: 4,
                            poolTimeout: Number.POSITIVE_INFINITY
                        }
                    },
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            envName: 'development'
                        }
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            happyPackMode: true
                        }
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|gif|woff2?|eot|ttf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    plugins: [
        new HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'Hyperthread'
        }),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
        }),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true
        })
    ]
};

export default config;