import {BaseComponent} from './base-component';
import React from 'react';
import styled from 'styled-components';

const StyledApp = styled.div`
  font-weight: bold;
`;

export class App extends BaseComponent
{
    public render(): React.ReactNode
    {
        return <StyledApp>Hello there</StyledApp>;
    }
}