import {Configuration} from 'webpack';

import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
// @ts-ignore
import CircularDependencyPlugin from 'circular-dependency-plugin';
import path from 'path';
// @ts-ignore
import createElectronReloadWebpackPlugin from 'electron-reload-webpack-plugin';

const distDir = path.join(__dirname, '../../dist');
const distFile = 'main.js';

const ElectronReloadWebpackPlugin = createElectronReloadWebpackPlugin({
    path: path.join(distDir, distFile),
});

const config: Configuration = {
    target: 'electron-main',

    resolve: {
        extensions: ['.ts', '.js'],
    },
    
    entry: './src/index.ts',
    devtool: 'eval-source-map',
    mode: 'development',
    output: {
        path: distDir,
        filename: distFile
    },

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    { loader: 'cache-loader' },
                    {
                        loader: 'thread-loader',
                        options: {
                            workers: 4,
                            poolTimeout: Number.POSITIVE_INFINITY
                        }
                    },
                    {
                        loader: 'ts-loader',
                        options: {
                            transpileOnly: true,
                            happyPackMode: true
                        }
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|gif|woff2?|eot|ttf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    plugins: [
        ElectronReloadWebpackPlugin(),
        new ForkTsCheckerWebpackPlugin({
            checkSyntacticErrors: true,
        }),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true
        })
    ]
};

export default config;